# coding: utf-8
from __future__ import unicode_literals
import codecs
import yaml
import json

with open('stages_groups.yml') as f:
	data = yaml.load(f,Loader=yaml.SafeLoader)

with open('team.yml') as f:
	handles = yaml.load(f,Loader=yaml.SafeLoader)

HANDLES = dict()
for i in handles:
	name = i['name']
	handle = i['slug']
	HANDLES[name] = handle

def get_handles(item):
	for k, v in HANDLES.items():
		if k == item:
			return(v)	


def get_members(item):
	members = []
	managers = []
	# hardcoded keys; not all keys
	KEYS = ['pm', 'pmm', 'cm', 'backend_engineering_manager',
	            'frontend_engineering_manager','support', 'sets', 
	            'pdm', 'ux', 'uxr', 'tech_writer', 'tw_backup', 'appsec_engineer']
	if isinstance(item, dict):
		for k, v in item.items():
			if k in KEYS:
				# check if key is manager
				if "engineering_manager" in k:
					managers.append({'member_name': item[k], 'member_handle': get_handles(item[k])})
				# check if item is a list
				elif isinstance(item[k], list):
					for x in item[k]:
						# skip garbage values
						if 'TBD' not in x:
							members.append(
								{'member_name': x, 'member_handle': get_handles(x)})
				else: # standard item
					if 'TBD' not in item[k]:
						members.append(
							{'member_name': item[k], 'member_handle': get_handles(item[k])})
					elif k in managers: 
						pass #prevents dupliate entries from managers
	return [managers,members]


prefix = f"eng-dev" # could be modified per department
member_list = []
# drill down into stages
for stage in data['stages']:
	# iterate over all stages to collect the 
	# different groups within each stage
	groups = data['stages'][stage]['groups'].keys()
	for group in data['stages'][stage]['groups']:
		# start building member list
		members = {'group_name': f"{prefix}-{stage}-{group}",'group_managers':[], 'group_members':[]}
		# iterate over each object in each group
		# extract the team member name
		# append to dictionary of group members
		for item in data['stages'][stage]['groups'][group]:
			members['group_managers'],members['group_members']= get_members(data['stages'][stage]['groups'][group])
		if not members['group_members']:
			break
		member_list.append(members)

print(json.dumps(member_list,ensure_ascii=False))